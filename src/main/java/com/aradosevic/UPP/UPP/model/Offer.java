package com.aradosevic.UPP.UPP.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Offer {

    @Id
    @GeneratedValue
    private Long id;

    private long price;

    private Date finishDate;

    private String state;

    private Long userId;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Request request;

    public Offer() {
    }

    public Offer(long price, Date finishDate, String state, Long userId, Request request) {
        this.price = price;
        this.finishDate = finishDate;
        this.state = state;
        this.userId = userId;
        this.request = request;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "id=" + id +
                ", price=" + price +
                ", finishDate=" + finishDate +
                ", state='" + state + '\'' +
                ", userId=" + userId +
                ", request=" + request +
                '}';
    }
}
