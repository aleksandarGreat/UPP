package com.aradosevic.UPP.UPP.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Request {

    @Id
    @GeneratedValue
    private Long id;

    private String category;

    private String details;

    private long maxPriceValue;

    private Date dateRecivingBids;

    private long maxNumberOfBids;

    private Date dateFinishPurchase;

    @OneToMany(mappedBy = "request", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Offer> offers;

    public Request() {
    }

    public Request(String category, String details, long maxPriceValue, Date dateRecivingBids, long maxNumberOfBids, Date dateFinishPurchase, List<Offer> offers) {
        this.category = category;
        this.details = details;
        this.maxPriceValue = maxPriceValue;
        this.dateRecivingBids = dateRecivingBids;
        this.maxNumberOfBids = maxNumberOfBids;
        this.dateFinishPurchase = dateFinishPurchase;
        this.offers = offers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public long getMaxPriceValue() {
        return maxPriceValue;
    }

    public void setMaxPriceValue(long maxPriceValue) {
        this.maxPriceValue = maxPriceValue;
    }

    public Date getDateRecivingBids() {
        return dateRecivingBids;
    }

    public void setDateRecivingBids(Date dateRecivingBids) {
        this.dateRecivingBids = dateRecivingBids;
    }

    public long getMaxNumberOfBids() {
        return maxNumberOfBids;
    }

    public void setMaxNumberOfBids(long maxNumberOfBids) {
        this.maxNumberOfBids = maxNumberOfBids;
    }

    public Date getDateFinishPurchase() {
        return dateFinishPurchase;
    }

    public void setDateFinishPurchase(Date dateFinishPurchase) {
        this.dateFinishPurchase = dateFinishPurchase;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", details='" + details + '\'' +
                ", maxPriceValue=" + maxPriceValue +
                ", dateRecivingBids=" + dateRecivingBids +
                ", maxNumberOfBids=" + maxNumberOfBids +
                ", dateFinishPurchase=" + dateFinishPurchase +
                ", offers=" + offers +
                '}';
    }
}
