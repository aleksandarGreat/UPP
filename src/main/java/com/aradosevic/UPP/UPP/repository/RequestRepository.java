package com.aradosevic.UPP.UPP.repository;

import com.aradosevic.UPP.UPP.model.Request;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestRepository extends JpaRepository<Request, Long> {
}
