package com.aradosevic.UPP.UPP.repository;

import com.aradosevic.UPP.UPP.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
