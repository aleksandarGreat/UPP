package com.aradosevic.UPP.UPP.repository;

import com.aradosevic.UPP.UPP.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {

    @Query("select user from User user where user.username = :username or user.email = :email")
    public List<User> findByUsernameAndMail(@Param("username") String username, @Param("email") String email);

    @Query("select user from User user where user.username = :username and user.password = :password and user.verificated = 1")
    public List<User> findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    @Query("select user from User user where user.category = :category")
    public List<User> findByCategory(@Param("category") String category);

    @Query("select user from User user where user.username = :username")
    public List<User> findByUsername(@Param("username") String username);
}


