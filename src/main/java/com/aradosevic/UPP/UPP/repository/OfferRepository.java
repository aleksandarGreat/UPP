package com.aradosevic.UPP.UPP.repository;

import com.aradosevic.UPP.UPP.model.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfferRepository extends JpaRepository<Offer,Long> {
}
