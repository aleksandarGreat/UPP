package com.aradosevic.UPP.UPP.repository;

import com.aradosevic.UPP.UPP.model.Rate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateRepository extends JpaRepository<Rate,Long> {
}
