package com.aradosevic.UPP.UPP.service.model;

import com.aradosevic.UPP.UPP.model.User;
import com.aradosevic.UPP.UPP.repository.UserRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Bakir Niksic on 1/28/2018.
 */
@Service
public class UserService{

    @Autowired
    private UserRepository userRepository;

    public User findOne(Long id){return userRepository.findOne(id);}

    public User save(User user){
        String address= user.getAdress()  + ", " + user.getCity() + ", Srbija";
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyA74dmDSIsfMCsaRN0wZkIu_qwfDBvIcHQ")
                .build();

        try {
            GeocodingResult[] results =  GeocodingApi.geocode(context, address).await();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            System.out.println(gson.toJson(results[0].geometry.location.lat));
            System.out.println(gson.toJson(results[0].geometry.location.lng));
            user.setLatitude(gson.toJson(results[0].geometry.location.lat));
            user.setLongitude(gson.toJson(results[0].geometry.location.lng));
        }catch (Exception e){
            System.err.println("GEO COORDINATES ERROR : ");
            e.printStackTrace();
        }

        return userRepository.save(user);
    }

    public void delete(Long id){userRepository.delete(id);}

    public List<User> findAll(){return userRepository.findAll();}

    public boolean isValid(String username, String mail) {
        return userRepository.findByUsernameAndMail(username, mail).size() <= 0;
    }

    public boolean doesUserExist(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, password).size() == 1;
    }

    public List<User> findByCategory(String category){
        return userRepository.findByCategory(category);
    }

    public List<User> findByUsername(String username){
        return userRepository.findByUsername(username);
    }
}
