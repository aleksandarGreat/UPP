package com.aradosevic.UPP.UPP.service.activiti;


import com.aradosevic.UPP.UPP.model.User;
import com.aradosevic.UPP.UPP.utils.StaticResources;

import java.io.Serializable;

public class RegistrationService implements Serializable {

    public boolean validateEntry(String username, String email){
        return StaticResources.userService.isValid(username, email);
    }

    public String addCategory(String categories, String newCategory){
        if(categories == null){
            categories = newCategory;
            System.err.println("new Category" + categories);
            return categories;
        }
        String [] categoriesArray = categories.split(";");
        for (String category : categoriesArray){
            if(category.equals(newCategory)){
                System.err.println("IMA VEC " + categories);
                return categories;
            }
        }
        categories += ";" + newCategory;
        System.err.println("RET VAL " + categories);
        return  categories;
    }

    public Long saveUserAndSendMail(String name, String username, String password, String email, String adress, String postalCode, String city, String category, Long distance, String confirmUser){
        User newUser = new User(name, username, password, email, adress, postalCode, city, category, false);
        newUser.setDistance(distance);
        newUser = StaticResources.userService.save(newUser);

        sendMail(email, confirmUser);
        return newUser.getId();
    }

    private void sendMail(String mailTo, String assignTo){
        StaticResources.emailService.sendMail(mailTo,"Verification","You need to verify your account! Please click: http://localhost:4200/tasks?user=" + assignTo);
    }

    public void clearemporaryData(Long id){
        StaticResources.userService.delete(id);
        System.out.println("Cleared temp data for user.");
    }

    public void finishRegistrationProcess(Long id){
        User user = StaticResources.userService.findOne(id);
        user.setVerificated(true);
        StaticResources.userService.save(user);
    }
}
