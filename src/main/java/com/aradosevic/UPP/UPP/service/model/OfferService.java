package com.aradosevic.UPP.UPP.service.model;

import com.aradosevic.UPP.UPP.model.Offer;
import com.aradosevic.UPP.UPP.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferService {

    public static String ACCEPT_STATE = "accept";
    public static String CANCEL_STATE = "cancel";

    @Autowired
    private OfferRepository offerRepository;

    public Offer findOne(Long id){return offerRepository.findOne(id);}

    public List<Offer> findAll(){return offerRepository.findAll();}

    public Offer save(Offer offer){return offerRepository.save(offer);}

    public void delete(Long id){offerRepository.delete(id);}
}
