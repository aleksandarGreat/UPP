package com.aradosevic.UPP.UPP.service.activiti;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendMail(String mailTo){
        try {
            SimpleMailMessage emailSMM = new SimpleMailMessage();
            emailSMM.setFrom("uppftn@gmail.com");
            emailSMM.setTo(mailTo);
            emailSMM.setSubject("Test");
            emailSMM.setText("Testing");
            javaMailSender.send(emailSMM);
        } catch (Exception ex) {
            System.out.println("Email is not sent." + ex.getMessage());
        }
    }

    public void sendMail(String mailTo, String subject, String text){
        try {
            SimpleMailMessage emailSMM = new SimpleMailMessage();
            emailSMM.setFrom("uppftn@gmail.com");
            emailSMM.setTo(mailTo);
            emailSMM.setSubject(subject);
            emailSMM.setText(text);
            javaMailSender.send(emailSMM);
            System.err.println("EMAIL SENT TO : "+ mailTo);
        } catch (Exception ex) {
            System.out.println("Email is not sent." + ex.getMessage());
        }
    }


}

