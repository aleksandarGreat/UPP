package com.aradosevic.UPP.UPP.service.model;

import com.aradosevic.UPP.UPP.model.Request;
import com.aradosevic.UPP.UPP.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {

    @Autowired
    private RequestRepository requestRepository;

    public Request findOne(Long id){return requestRepository.findOne(id);}

    public List<Request> findAll(){return requestRepository.findAll();}

    public Request save(Request request){return requestRepository.save(request);}

    public void delete(Long id){requestRepository.delete(id);}
}
