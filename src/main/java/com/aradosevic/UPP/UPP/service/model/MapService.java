package com.aradosevic.UPP.UPP.service.model;

import com.aradosevic.UPP.UPP.model.User;
import org.springframework.stereotype.Service;

@Service
public class MapService {

    public Double getDistance(User user1, User user2){
        Integer R = 6371;
        Double latitude1 = Double.parseDouble(user1.getLatitude());
        Double longitude1 = Double.parseDouble(user1.getLongitude());
        Double latitude2 = Double.parseDouble(user2.getLatitude());
        Double longitude2 = Double.parseDouble(user2.getLongitude());

        Double dLat = deg2rad(latitude2-latitude1);  // deg2rad below
        Double dLon = deg2rad(longitude2-longitude1);


        Double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(latitude1)) * Math.cos(deg2rad(latitude2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);

        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double d = R * c; // Distance in km
        return d;

    }

    public Double deg2rad(Double deg) {
        return deg * (Math.PI/180);
    }

}
