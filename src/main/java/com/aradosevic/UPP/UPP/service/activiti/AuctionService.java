package com.aradosevic.UPP.UPP.service.activiti;

import com.aradosevic.UPP.UPP.model.Offer;
import com.aradosevic.UPP.UPP.model.Rate;
import com.aradosevic.UPP.UPP.model.Request;
import com.aradosevic.UPP.UPP.model.User;
import com.aradosevic.UPP.UPP.utils.StaticResources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AuctionService implements Serializable {

    public List<User> findAffectedCompanies(String category, Long maxNumberOfBids, Long restartNumber, String username){
        //List<User> userList = StaticResource.userService.findByCategory(category);
        User userx = StaticResources.userService.findByUsername(username).get(0);
        List<User> userList = StaticResources.userService.findAll();
        System.err.println("CATEGORY : "+category);
        System.err.println("SIZE OF LIST BEFORE REMOVING IS" + userList.size());
        for(int i=userList.size()-1; i>=0 ; i--){
            System.err.println(userList.get(i).getUsername()+":"+userList.get(i).getCategory());
            System.err.println("DISTANCE IS : "+StaticResources.mapService.getDistance(userx, userList.get(i)));
            if(userList.get(i).getCategory() == null || !userList.get(i).getCategory().contains(category)
                    || StaticResources.mapService.getDistance(userx, userList.get(i)) > userList.get(i).getDistance()){
                System.err.println(userList.get(i).getUsername()+" is removed");
                double maxDistance = userList.get(i).getDistance();
                double currentDistance = StaticResources.mapService.getDistance(userx, userList.get(i));
                userList.remove(i);
            }
            System.err.println(".");
        }

        System.err.println("SIZE : "+userList.size());
        System.err.println("MAX : "+maxNumberOfBids);
        ArrayList<User> retVal = new ArrayList<>();
        int counter = Integer.parseInt(restartNumber.toString());
        int max = Integer.parseInt(maxNumberOfBids.toString());

        int i = counter*max -1;
        for(User user : userList){
            if(userList.indexOf(user) <= i){
                continue;
            }
            if(retVal.size() == maxNumberOfBids){
                break;
            }
            retVal.add(user);
        }
        System.err.println("RET SIZE : "+retVal.size());
        return retVal;
    }

    public void noticeThereAreNoCompanies(String username){
        String email = StaticResources.userService.findByUsername(username).get(0).getEmail();
        StaticResources.emailService.sendMail(email, "Notification", "There is no company that performs a given category");
    }
    public void noticeThereAreLessCompanies(String username){
        String email = StaticResources.userService.findByUsername(username).get(0).getEmail();
        StaticResources.emailService.sendMail(email, "Notification", "There is less than expected number of companies that performs a given category");
    }

    public void notifyCompaniesCancelation(String username) {
        String email = StaticResources.userService.findByUsername(username).get(0).getEmail();
        StaticResources.emailService.sendMail(email, "Notification", "This auction has been canceled.");
    }

    public void notifyEveryCompany(String email){
        StaticResources.emailService.sendMail(email, "Notification", "Please, accept or cancel your offer");
    }

    public long calculateRank(Long requestId, long price, Date finishDate){
        System.err.println("Calculate rank");
        Request request = StaticResources.requestService.findOne(requestId);
        if(request == null){
            System.err.println("REQUEST WITH ID : " + requestId + " DOES NOT EXIST");
            return -1;
        }
        //TODO: implement this algorithm better, priority is now only price
        long currentRank = 1;
        for(Offer offer : StaticResources.requestService.findOne(requestId).getOffers()){
            if(offer.getState().equals(StaticResources.offerService.CANCEL_STATE)){
                continue;
            }
            System.err.println(offer.getPrice() + " < " + price);
            if(offer.getPrice() < price){
                currentRank++;
                System.err.println(currentRank + " current rank");
            }
        }
        return currentRank;
    }

    public void saveOffer(Long requestId, long price, Date finishDate, Long userId){
        Offer offer = new Offer();
        offer.setPrice(price);
        offer.setFinishDate(finishDate);
        offer.setState(StaticResources.offerService.ACCEPT_STATE);
        offer.setUserId(userId);
        offer.setRequest(StaticResources.requestService.findOne(requestId));
        Request request = StaticResources.requestService.findOne(requestId);
        request.getOffers().add(offer);
        StaticResources.requestService.save(request);
        System.err.println("Before : " + StaticResources.requestService.findOne(requestId).getOffers().size() + " offers");
        //offer = StaticResource.offerService.save(offer);
        System.err.println("Offer (price:" + price + " finishDate : " + finishDate + "state : " + offer.getState() + ") saved with requestId : " + requestId + "and user id: " + userId);
        System.err.println("Current i database : " + StaticResources.requestService.findOne(requestId).getOffers().size() + " offers");
        for(Offer offerx : StaticResources.requestService.findOne(requestId).getOffers()){
            System.err.println(StaticResources.userService.findOne(offerx.getUserId()));
        }
    }

    public Long saveRequest(String category, String details, long maxPriceValue,
                            Date dateReceivingBids, long maxNumberOfBids, Date dateFinishPurchase){
        Request request = new Request();
        request.setCategory(category);
        request.setDetails(details);
        request.setMaxPriceValue(maxPriceValue);
        request.setDateRecivingBids(dateReceivingBids);
        request.setMaxNumberOfBids(maxNumberOfBids);
        request.setDateFinishPurchase(dateFinishPurchase);
        request = StaticResources.requestService.save(request);
        return request.getId();
    }

    public List<Offer> offersAnalyse(Long requestId){
        System.err.println("--offersAnalyse");
        ArrayList<Offer> retVal = new ArrayList<>();
        for(Offer offer : StaticResources.requestService.findOne(requestId).getOffers()){
            System.err.println("ITERATOR FOR ANALYSE");
            if(offer.getState().equals(StaticResources.offerService.ACCEPT_STATE)){
                System.err.println("ADD--offersAnalyse "+StaticResources.userService.findOne(offer.getUserId()).getName());
                retVal.add(offer);
            }
        }
        return retVal;
    }

    public void doNothing(){
        System.err.println("DO NOTHING");

    }

    public void saveCanceledOffer(Long userId, Long requestId){
        Offer offer = new Offer();
        offer.setState(StaticResources.offerService.CANCEL_STATE);
        offer.setUserId(userId);
        offer.setRequest(StaticResources.requestService.findOne(requestId));
        Request request = StaticResources.requestService.findOne(requestId);
        request.getOffers().add(offer);
        StaticResources.requestService.save(request);
        System.err.println("Offer state : " + offer.getState() + ") saved with requestId : " + requestId + "and user id: " + userId);
    }

    public void notifyThatThereIsNoOffers(String username){
        String email = StaticResources.userService.findByUsername(username).get(0).getEmail();
        StaticResources.emailService.sendMail(email,"Notification", "There is no accepted offers, please extend deadline or finish process");
    }

    public void noticeThatThereIsLessThanExpectedNumbersOfOffers(String username){
        String email = StaticResources.userService.findByUsername(username).get(0).getEmail();
        StaticResources.emailService.sendMail(email,"Notification", "There is less than expected number of accepted offers, please extend deadline or continiue with this offers");
    }

    public List<User> chooseAnotherCompanies(Long requestId, List<User> affectedCompanies){
        ArrayList<User> retVal = new ArrayList<>();
        for (User company : affectedCompanies){
            boolean companyDoesNotExistInOffers = true;
            for(Offer offer : StaticResources.requestService.findOne(requestId).getOffers()){
                if(offer.getUserId() == company.getId()){
                    companyDoesNotExistInOffers = false;
                    break;
                }
            }
            if(companyDoesNotExistInOffers){
                System.err.println("Company added" + company.getId());
                retVal.add(company);
            }
        }
        return retVal;
    }

    public String updateTime(String date, String hours){
        String [] time = date.split("T");
        return time[0] + "T" + hours;
    }

    public String rangOffers(Long requestId){
        return requestId.toString();
    }

    public String findCompaniesAgent(String offer){
        System.err.println(offer);
        String [] offerString = offer.split("Username: ");
        String [] usernameString = offerString[1].split(" ");
        System.err.println(usernameString[0]);
        return usernameString[0];
    }

    public void notifyAboutConfirmation(String username){
        String email = StaticResources.userService.findByUsername(username).get(0).getEmail();
        StaticResources.emailService.sendMail(email, "Notification", " Your offer is choosen!");
    }

    public void saveRates(String clientRate, String companyRate, String client, String company){
        double clientRateDouble = Double.parseDouble(clientRate);
        double companyRateDouble = Double.parseDouble(companyRate);

        Rate rate = new Rate();
        rate.setRate(clientRateDouble);
        rate.setGrader(client);
        rate.setRated(company);
        StaticResources.rateService.save(rate);

        Rate rate2 = new Rate();
        rate2.setRate(companyRateDouble);
        rate2.setGrader(company);
        rate2.setRated(client);
        StaticResources.rateService.save(rate2);
    }

    public long incrementRestart(long counter){
        System.err.println(counter + 1 + " counter ");
        return counter + 1;
    }

    public String notifyRequestor(String username ,ArrayList<Offer> acceptedOffers) {
        String email = StaticResources.userService.findByUsername(username).get(0).getEmail();
        if (acceptedOffers.size() == 0) {
            String message = "There is no company that performs a given category";
            StaticResources.emailService.sendMail(email,"Notification", message);
            return message;
        } else {
            String message = "There is less than expected number of accepted offers, please extend deadline or continiue with this offers";
            StaticResources.emailService.sendMail(email,"Notification", message);
            return message;
        }
    }

}
