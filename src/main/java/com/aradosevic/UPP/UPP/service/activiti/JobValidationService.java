package com.aradosevic.UPP.UPP.service.activiti;

import org.springframework.stereotype.Component;

@Component
public class JobValidationService {

    public int initOffer() {
        System.out.println("Job offer initialized.");

        return 2;
    }

    public void notEnoughFirms() {
        System.out.println("Not enough firms.");
    }

    public void enoughFirms() {
        System.out.println("Enough firms.");
    }
}
