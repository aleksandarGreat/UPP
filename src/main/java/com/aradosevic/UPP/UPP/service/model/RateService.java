package com.aradosevic.UPP.UPP.service.model;

import com.aradosevic.UPP.UPP.model.Rate;
import com.aradosevic.UPP.UPP.repository.RateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RateService {

    @Autowired
    private RateRepository rateRepository;

    public Rate findOne(Long id){return rateRepository.findOne(id);}

    public List<Rate> findAll(){return rateRepository.findAll();}

    public Rate save(Rate rate){return rateRepository.save(rate);}

    public void delete(Long id){rateRepository.delete(id);}
}
