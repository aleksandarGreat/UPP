package com.aradosevic.UPP.UPP.service.model;

import com.aradosevic.UPP.UPP.dto.FormPropertiesDTO;
import com.aradosevic.UPP.UPP.dto.TaskDTO;
import com.aradosevic.UPP.UPP.model.Category;
import com.aradosevic.UPP.UPP.model.Offer;
import com.aradosevic.UPP.UPP.service.ProcessService;
import com.aradosevic.UPP.UPP.utils.EnumItem;
import com.aradosevic.UPP.UPP.utils.StaticResources;
import org.activiti.engine.FormService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TaskService {

    @Autowired
    private ProcessService processService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private UserService userService;

    public TaskService() {
    }

    public List<TaskDTO> getAllTasks(String assigner){
        org.activiti.engine.TaskService taskService = processService.getProcessEngine().getTaskService();
        ArrayList<TaskDTO> retValue = new ArrayList<>();
        for(Task task : taskService.createTaskQuery().taskAssignee(assigner).list()){
            retValue.add(new TaskDTO(task.getName(), task.getId()));
        }
        return retValue;
    }

    public List<FormPropertiesDTO> getFormProperties(String taskId){
        ArrayList<FormPropertiesDTO> retValue = new ArrayList<>();
        FormService formService = processService.getProcessEngine().getFormService();
        int i=0;
        for(org.activiti.engine.form.FormProperty formPropertyInterface : formService.getTaskFormData(taskId).getFormProperties()){
            FormPropertiesDTO myFormProperty = new FormPropertiesDTO();
            myFormProperty.setLabel(formPropertyInterface.getName());
            myFormProperty.setName(formPropertyInterface.getId());
            myFormProperty.setValue(formPropertyInterface.getValue());
            System.out.println(i++ + " Possible: "+ formPropertyInterface.getType()) ;

            if(formPropertyInterface.getType().getName().equals("string")){
                if (formPropertyInterface.getId().equals("categories")) {
                        myFormProperty.setType("multi");
                        ArrayList<EnumItem> arrayListEnumItems = new ArrayList<>();
                        for(Category category : categoryService.findAll()){
                            arrayListEnumItems.add(new EnumItem(category.getActivitiId(), category.getName()));
                        }
                        myFormProperty.setValues(arrayListEnumItems);
                } else if (formPropertyInterface.getId().equals("current_rang")) {
                    myFormProperty.setType("multi");
                    arrangeOfferDisplay(formPropertyInterface, myFormProperty);
                }
                else {
                    myFormProperty.setType("text");
                }
            }else{
                myFormProperty.setType(formPropertyInterface.getType().getName());
            }

            myFormProperty.setReadable(formPropertyInterface.isReadable());
            myFormProperty.setWritable(formPropertyInterface.isWritable());
            myFormProperty.setRequired(formPropertyInterface.isRequired());

            if(formPropertyInterface.getType().getName().equals("enum")){
                HashMap<String, String> hashMapString = (HashMap<String, String>) formPropertyInterface.getType().getInformation("values");
                System.out.println("Values of enumaration: " + hashMapString.size());
                ArrayList<EnumItem> arrayListString = new ArrayList<>();
                for (String keyValue : hashMapString.keySet()) {
                    arrayListString.add(new EnumItem(keyValue, hashMapString.get(keyValue)));
                }
                myFormProperty.setValues(arrayListString);
            }

            if(formPropertyInterface.getId().startsWith("enum_")){
                myFormProperty.setType("enum");
                ArrayList<EnumItem> arrayListEnumItems = new ArrayList<>();
                if(formPropertyInterface.getId().contains("enum_category")){
                    for(Category category : categoryService.findAll()){
                        arrayListEnumItems.add(new EnumItem(category.getActivitiId(), category.getName()));
                    }
                    myFormProperty.setValues(arrayListEnumItems);
                }else if(formPropertyInterface.getId().contains("enum_rang")){
                    arrangeOfferDisplay(formPropertyInterface, myFormProperty);
                }
            }

            retValue.add(myFormProperty);
        }
        return retValue;
    }

    private void arrangeOfferDisplay(FormProperty formPropertyInterface, FormPropertiesDTO myFormProperty) {
        ArrayList<EnumItem> arrayListEnumItems = new ArrayList<>();
        List<Offer> offers = requestService.findOne(Long.parseLong(formPropertyInterface.getValue())).getOffers();
        for(int k = 0 ; k < offers.size(); k++){
            for(int z = 1 ; z<offers.size(); z++) {
                if(offers.get(z).getPrice() < offers.get(k).getPrice()) {
                    Offer tempOffer = offers.get(k);
                    offers.set(k,offers.get(z));
                    offers.set(z,tempOffer);
                }
            }
        }
        int counter = 1;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        for(Offer offer : offers){
            if(offer.getState().equals(StaticResources.offerService.ACCEPT_STATE)){
                String date = formatter.format(offer.getFinishDate());
                arrayListEnumItems.add(new EnumItem("offer" + offer.getId(),
                        counter + ". Price: "+offer.getPrice() +
                                "$ Username: " + userService.findOne(offer.getUserId()).getUsername() +
                                " Finish Date: " + date
                ));
                counter++;
            }
        }
        myFormProperty.setValues(arrayListEnumItems);
    }

    public void postFormPropertiesOnTaskAndExecute(String taskId, List<FormPropertiesDTO> formPropetiesJson){
        Map<String, String> submitData = new HashMap<>();
        for(FormPropertiesDTO formPropertyJson : formPropetiesJson){
            if(formPropertyJson.isWritable()){
                if(formPropertyJson.getType().equals("enum") && !formPropertyJson.getName().startsWith("enum_")){
                    for(int i=0;i<formPropertyJson.getValues().size(); i++){
                        if(formPropertyJson.getValues().get(i).getValue().equals(formPropertyJson.getValue())){
                            submitData.put(formPropertyJson.getName(), formPropertyJson.getValues().get(i).getName());
                            break;
                        }
                    }
                } else if (formPropertyJson.getType().equals("multi")){
                    if (!formPropertyJson.getMulti().isEmpty()) {
                        StringBuilder categories = null;
                        for (String enumItem: formPropertyJson.getMulti()) {
                            if (categories == null) {
                                categories = new StringBuilder(enumItem);
                            } else {
                                categories.append(";").append(enumItem);
                            }
                        }
                        submitData.put(formPropertyJson.getName(), categories.toString());
                    }
                }
                else{
                    submitData.put(formPropertyJson.getName(), formPropertyJson.getValue());
                }
            }
        }
        FormService formService = processService.getProcessEngine().getFormService();
        formService.submitTaskFormData(taskId, submitData);
    }
}
