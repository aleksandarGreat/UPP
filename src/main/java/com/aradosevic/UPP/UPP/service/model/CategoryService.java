package com.aradosevic.UPP.UPP.service.model;

import com.aradosevic.UPP.UPP.model.Category;
import com.aradosevic.UPP.UPP.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Category findOne(Long id){return categoryRepository.findOne(id);}

    public List<Category> findAll(){return categoryRepository.findAll();}

    public Category save(Category category){return categoryRepository.save(category);}

    public void delete(Long id){categoryRepository.delete(id);}
}
