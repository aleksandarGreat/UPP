package com.aradosevic.UPP.UPP.service.activiti;

import org.springframework.stereotype.Component;

@Component
public class ValidationService {

    public void validate(){
        System.out.println("I made it here.");
    }

    public void skipThis() {
        System.out.println("I have skipped this.");
    }
}