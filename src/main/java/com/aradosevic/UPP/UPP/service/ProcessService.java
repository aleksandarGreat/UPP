package com.aradosevic.UPP.UPP.service;

import com.aradosevic.UPP.UPP.dto.ProcessDTO;
import com.aradosevic.UPP.UPP.service.activiti.RegistrationService;
import com.aradosevic.UPP.UPP.service.activiti.AuctionService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class ProcessService {

    @Value("${registrationProcessResourcePath}")
    private String registrationProcessResourcePath;

    @Value("${tenderProcessResourcePath}")
    private String tenderProcessResourcePath;

    private ProcessEngine processEngine;

    public ProcessService() {
    }

    public void initialize(ProcessEngine processEngine){
        this.processEngine = processEngine;
        deploy();
    }

    private void deploy(){
        RepositoryService repositoryService = processEngine.getRepositoryService();
        for (Deployment d : repositoryService.createDeploymentQuery().list())
        {
            repositoryService.deleteDeployment(d.getId(), true);
        }
        repositoryService.createDeployment().addClasspathResource(registrationProcessResourcePath).deploy();
        repositoryService.createDeployment().addClasspathResource(tenderProcessResourcePath).deploy();
    }


    public ProcessDTO startRegistrationProcess(){
        String userUUID = UUID.randomUUID().toString();
        String confirmUUID = UUID.randomUUID().toString();

        Map<String, Object> variables = new HashMap<>();

        variables.put("registrationService", new RegistrationService());
        variables.put("registrationIssuer", "registrationIssuer" + userUUID);
        variables.put("verifier", "verifier" + confirmUUID);
        variables.put("categories", null);
        variables.put("distance", 0L);

        RuntimeService runtimeService = processEngine.getRuntimeService();
        System.out.println("Before registration started process: " + runtimeService.createProcessInstanceQuery().count());
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("registration", variables);
        return new ProcessDTO("registrationIssuer"+userUUID, processInstance.getId());
    }

    public ProcessDTO startTenderProcess(String initiator){

        Map<String, Object> variables = new HashMap<>();
        variables.put("auctionService", new AuctionService());
        variables.put("auctioneer", initiator);
        variables.put("restartNumber", 0L);
        RuntimeService runtimeService = processEngine.getRuntimeService();
        System.out.println("Before tender started process: " + runtimeService.createProcessInstanceQuery().count());
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("auction", variables);
        return new ProcessDTO(initiator, processInstance.getId());
    }

    public ProcessEngine getProcessEngine() {
        return processEngine;
    }

    public void setProcessEngine(ProcessEngine processEngine) {
        this.processEngine = processEngine;
    }
}
