package com.aradosevic.UPP.UPP.dto;

import com.aradosevic.UPP.UPP.enumerations.JobCategory;

public class JobInitDTO {

    private JobCategory jobCategory;

    private String jobDescription;

    private String maxValue;

    private String finalOfferDate;

    private String maxOffers;

    private String finalizationDate;

    public JobInitDTO() {
    }

    public JobInitDTO(JobCategory jobCategory, String jobDescription, String maxValue, String finalOfferDate, String maxOffers, String finalizationDate) {
        this.jobCategory = jobCategory;
        this.jobDescription = jobDescription;
        this.maxValue = maxValue;
        this.finalOfferDate = finalOfferDate;
        this.maxOffers = maxOffers;
        this.finalizationDate = finalizationDate;
    }

    public JobCategory getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(JobCategory jobCategory) {
        this.jobCategory = jobCategory;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getFinalOfferDate() {
        return finalOfferDate;
    }

    public void setFinalOfferDate(String finalOfferDate) {
        this.finalOfferDate = finalOfferDate;
    }

    public String getMaxOffers() {
        return maxOffers;
    }

    public void setMaxOffers(String maxOffers) {
        this.maxOffers = maxOffers;
    }

    public String getFinalizationDate() {
        return finalizationDate;
    }

    public void setFinalizationDate(String finalizationDate) {
        this.finalizationDate = finalizationDate;
    }

    @Override
    public String toString() {
        return "JobInitDTO{" +
                "jobCategory=" + jobCategory +
                ", jobDescription='" + jobDescription + '\'' +
                ", maxValue=" + maxValue +
                ", finalOfferDate=" + finalOfferDate +
                ", maxOffers=" + maxOffers +
                ", finalizationDate=" + finalizationDate +
                '}';
    }
}
