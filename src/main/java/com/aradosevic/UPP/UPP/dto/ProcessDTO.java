package com.aradosevic.UPP.UPP.dto;

public class ProcessDTO {

    private String tempUser;
    private String processId;

    public ProcessDTO(){
    }

    public ProcessDTO(String tempUser, String processId) {
        this.tempUser = tempUser;
        this.processId = processId;
    }

    public String getTempUser() {
        return tempUser;
    }

    public void setTempUser(String tempUser) {
        this.tempUser = tempUser;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    @Override
    public String toString() {
        return "ProcessDTO{" +
                "tempUser='" + tempUser + '\'' +
                ", processId='" + processId + '\'' +
                '}';
    }
}
