package com.aradosevic.UPP.UPP.dto;

public class RegisterDTO {

    private String pass;

    private String username;

    public RegisterDTO() {
    }

    public RegisterDTO(String pass, String username) {
        this.pass = pass;
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String name) {
        this.pass = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "RegisterDTO{" +
                "pass='" + pass + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
