package com.aradosevic.UPP.UPP.dto;

public class LoginDTO {

    private String username;
    private String password;
    private boolean logStatus;

    public LoginDTO() {
    }

    public LoginDTO(String username, String password, boolean logStatus) {
        this.username = username;
        this.password = password;
        this.logStatus = logStatus;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLogStatus() {
        return logStatus;
    }

    public void setLogStatus(boolean logStatus) {
        this.logStatus = logStatus;
    }

    @Override
    public String toString() {
        return "LoginDTO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", logStatus=" + logStatus +
                '}';
    }
}
