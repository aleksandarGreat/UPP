package com.aradosevic.UPP.UPP.dto;

import com.aradosevic.UPP.UPP.utils.EnumItem;

import java.util.ArrayList;

public class FormPropertiesDTO {

    private String type;

    private String name;

    private String label;

    private String value;

    private boolean required;

    private boolean writable;

    private boolean readable;

    private ArrayList<EnumItem> values;

    private ArrayList<String> multi;

    public FormPropertiesDTO() {
    }

    public FormPropertiesDTO(String type, String name, String label, String value, boolean required, boolean writable, boolean readable) {
        this.type = type;
        this.name = name;
        this.label = label;
        this.value = value;
        this.required = required;
        this.writable = writable;
        this.readable = readable;
        this.values = values;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isWritable() {
        return writable;
    }

    public void setWritable(boolean writable) {
        this.writable = writable;
    }

    public boolean isReadable() {
        return readable;
    }

    public void setReadable(boolean readable) {
        this.readable = readable;
    }

    public ArrayList<EnumItem> getValues() {
        return values;
    }

    public void setValues(ArrayList<EnumItem> values) {
        this.values = values;
    }

    public ArrayList<String> getMulti() {
        return multi;
    }

    public void setMulti(ArrayList<String> multi) {
        this.multi = multi;
    }

    @Override
    public String toString() {
        return "FormPropertiesDTO{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", label='" + label + '\'' +
                ", value='" + value + '\'' +
                ", required=" + required +
                ", writable=" + writable +
                ", readable=" + readable +
                ", values=" + values +
                '}';
    }
}
