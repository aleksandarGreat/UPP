package com.aradosevic.UPP.UPP.controller;

import com.aradosevic.UPP.UPP.dto.ProcessDTO;
import com.aradosevic.UPP.UPP.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/process")
public class ProcessController {

    @Autowired
    private ProcessService processService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ResponseEntity<ProcessDTO> startRegistrationProcess(){
        return new ResponseEntity<>(processService.startRegistrationProcess(), HttpStatus.OK);
    }

    @CrossOrigin("*")
    @RequestMapping(value = "/tender", method = RequestMethod.POST)
    public ResponseEntity<ProcessDTO> startTenderProcess(@RequestBody String initiator){
        if(initiator == null){
            return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(processService.startTenderProcess(initiator), HttpStatus.OK);
    }
}
