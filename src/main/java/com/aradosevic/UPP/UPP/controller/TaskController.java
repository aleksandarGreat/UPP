package com.aradosevic.UPP.UPP.controller;

import com.aradosevic.UPP.UPP.dto.FormPropertiesDTO;
import com.aradosevic.UPP.UPP.dto.TaskDTO;
import com.aradosevic.UPP.UPP.service.model.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<TaskDTO>> getUserTasks(@PathVariable("userId") String userId){
        return new ResponseEntity<>(taskService.getAllTasks(userId), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/form/{taskId}", method = RequestMethod.GET)
    public ResponseEntity<List<FormPropertiesDTO>> getFormProperties(@PathVariable("taskId") String taskId){
        return new ResponseEntity<>(taskService.getFormProperties(taskId), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/form/{taskId}", method = RequestMethod.POST)
    public ResponseEntity<List<FormPropertiesDTO>> postFormPropetiesOnTaskAndExecute(@PathVariable("taskId") String taskId, @RequestBody List<FormPropertiesDTO> myFormProperties) {
        if (myFormProperties == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        taskService.postFormPropertiesOnTaskAndExecute(taskId, myFormProperties);

        return new ResponseEntity<>(myFormProperties, HttpStatus.OK);
    }
}
