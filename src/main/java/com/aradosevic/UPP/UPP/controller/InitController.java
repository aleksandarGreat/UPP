package com.aradosevic.UPP.UPP.controller;

import com.aradosevic.UPP.UPP.model.Category;
import com.aradosevic.UPP.UPP.model.User;
import com.aradosevic.UPP.UPP.service.ProcessService;
import com.aradosevic.UPP.UPP.service.activiti.EmailService;
import com.aradosevic.UPP.UPP.service.model.*;
import com.aradosevic.UPP.UPP.utils.StaticResources;
import org.activiti.engine.ProcessEngines;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

@Controller
public class InitController implements CommandLineRunner {

    @Autowired
    public InitController(MapService mapService, UserService userService, EmailService emailService,
                          OfferService offerService, RequestService requestService, RateService rateService) {
        StaticResources.mapService = mapService;
        StaticResources.userService = userService;
        StaticResources.emailService = emailService;
        StaticResources.offerService = offerService;
        StaticResources.requestService = requestService;
        StaticResources.rateService = rateService;
    }

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProcessService processService;

    @Autowired
    private UserService userService;

    @Override
    public void run(String... args) {
        processService.initialize(ProcessEngines.getDefaultProcessEngine());
        initCategories();
    }

    private void initCategories(){
        Category category1 = new Category("Developer","We are building houses","building");
        Category category2 = new Category("Tester","Sport","sport");
        Category category3 = new Category("Architect","We are teaching students","teaching");

        categoryService.save(category1);
        categoryService.save(category2);
        categoryService.save(category3);

        User user = new User();
        user.setVerificated(true);
        user.setAdress("Dragise Brasovana");
        user.setCategory(null);
        user.setCity("Novi Sad");
        user.setEmail("aaa@gmail.com");
        user.setName("aaa");
        user.setPassword("aaa");
        user.setUsername("aaa");
        user.setPostalCode("21000");
        user.setDistance(50);
        userService.save(user);

        User user2 = new User();
        user2.setVerificated(true);
        user2.setAdress("Kisacka");
        user2.setCategory("Tester");
        user2.setCity("Novi Sad");
        user2.setEmail("sss@yahoo.com");
        user2.setName("sss");
        user2.setPassword("sss");
        user2.setUsername("sss");
        user2.setPostalCode("21000");
        user2.setDistance(60);
        userService.save(user2);

        User user3 = new User();
        user3.setVerificated(true);
        user3.setAdress("Segedinski put");
        user3.setCategory("Tester");
        user3.setCity("Subotica");
        user3.setEmail("qqq@gmail.com");
        user3.setName("qqq");
        user3.setPassword("qqq");
        user3.setUsername("qqq");
        user3.setPostalCode("24000");
        user3.setDistance(70);
        userService.save(user3);

        User user4 = new User();
        user4.setVerificated(true);
        user4.setAdress("Vase Stajica");
        user4.setCategory("Developer;Tester;Architect");
        user4.setCity("Novi Sad");
        user4.setEmail("fff@yahoo.com");
        user4.setName("fff");
        user4.setPassword("fff");
        user4.setUsername("fff");
        user4.setPostalCode("21000");
        user4.setDistance(40);
        userService.save(user4);

        User user5 = new User();
        user5.setVerificated(true);
        user5.setAdress("Vase Stajica");
        user5.setCategory("Developer;Tester;Architect");
        user5.setCity("Novi Sad");
        user5.setEmail("ddd@yahoo.com");
        user5.setName("ddd");
        user5.setPassword("ddd");
        user5.setUsername("ddd");
        user5.setPostalCode("21000");
        user5.setDistance(60);
        userService.save(user5);

        User user6 = new User();
        user6.setVerificated(true);
        user6.setAdress("Vase Stajica");
        user6.setCategory("Developer;Tester;Architect");
        user6.setCity("Novi Sad");
        user6.setEmail("ggg@yahoo.com");
        user6.setName("ggg");
        user6.setPassword("ggg");
        user6.setUsername("ggg");
        user6.setPostalCode("21000");
        user6.setDistance(50);
        userService.save(user6);
    }
}
