package com.aradosevic.UPP.UPP.controller;

import com.aradosevic.UPP.UPP.dto.LoginDTO;
import com.aradosevic.UPP.UPP.service.model.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    private UserService userService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<LoginDTO> postLoginJson(@RequestBody LoginDTO loginModel) {
        if (loginModel == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        loginModel.setLogStatus(userService.doesUserExist(loginModel.getUsername(),loginModel.getPassword()));

        return new ResponseEntity<>(loginModel, HttpStatus.OK);
    }
}
