package com.aradosevic.UPP.UPP.utils;

import com.aradosevic.UPP.UPP.service.activiti.EmailService;
import com.aradosevic.UPP.UPP.service.model.*;

public class StaticResources {

    public static UserService userService;
    public static EmailService emailService;
    public static OfferService offerService;
    public static RequestService requestService;
    public static RateService rateService;
    public static MapService mapService;
}
