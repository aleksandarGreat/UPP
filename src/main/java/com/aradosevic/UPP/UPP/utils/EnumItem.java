package com.aradosevic.UPP.UPP.utils;

public class EnumItem {

    private String name;

    private String value;

    public EnumItem() {
    }

    public EnumItem(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "EnumItem{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
