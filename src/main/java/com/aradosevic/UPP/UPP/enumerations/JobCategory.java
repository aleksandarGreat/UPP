package com.aradosevic.UPP.UPP.enumerations;

public enum JobCategory {
    SOFTWARE_DEVELOPER,
    ARCHITECT
}
