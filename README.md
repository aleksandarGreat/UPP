# Business Process Management

University project mainly focused on successful implementation of process-driven development.
Works with .bpmn files with Spring Boot as support to creating a server side.

## Build

Using IntelliJ it can be run through its IDE or after build, jar or war files respectfully can be found in target folder.

## Front-end

This project represents only server side of the overall project, frontend can be found here:
https://gitlab.com/aleksandarGreat/UPP-front